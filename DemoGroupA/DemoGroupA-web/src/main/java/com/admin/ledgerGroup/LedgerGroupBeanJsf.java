/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.ledgerGroup;

import com.admin.dto.AdminDto;
import com.admin.dto.CollegeDto;
import com.admin.dto.LedgerGroupDto;
import com.admin.service.LedgerGroupService;
import com.admin.util.Utility;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author cc
 */
@Getter
@Setter
@ManagedBean
@RequestScoped
public class LedgerGroupBeanJsf {
    
    @ManagedProperty(value = "#{ledgerGroupDataModelJsf}")
    private LedgerGroupDataModelJsf ledgerGroupDataModelJsf;
    
     @EJB
    private LedgerGroupService ledgerGroupService;
     
    private CollegeDto collegeDto;
     
    private AdminDto adminDto;
    
    @PostConstruct
    public void init() {
        collegeDto = new CollegeDto();
        collegeDto.setId(1l);

        adminDto = new AdminDto();
        adminDto.setId(1L);

        adminDto.setCollegeDto(collegeDto);
    }
    
    
    public String returnToPage() {
        return "ledgerGroup.xhtml?faces-redirect=true";
    }
    
    public String initCreate() {
        ledgerGroupDataModelJsf.setLedgerGroupDto(new LedgerGroupDto());
        ledgerGroupDataModelJsf.setCreateEditPanel(true);
        return returnToPage();
    }
    
    public String saveUpdate() {
        ledgerGroupDataModelJsf.getLedgerGroupDto().setUpdatedByAdminDto(adminDto);
        ledgerGroupDataModelJsf.getLedgerGroupDto().setCreatedByAdminDto(adminDto);
        
        if (ledgerGroupService.checkIfLedgerGroupNameAlreadyExists(ledgerGroupDataModelJsf.getLedgerGroupDto())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "LedgerGroup Name Already Exists", null));
            return returnToPage();
        }
        if (ledgerGroupService.checkIfLedgerGroupCodeAlreadyExists(ledgerGroupDataModelJsf.getLedgerGroupDto())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "LedgerGroup Code Already Exists", null));
            return returnToPage();
        }

        if (ledgerGroupDataModelJsf.getLedgerGroupDto().getId() == null) {
            return save();
        } else {
            return update();
        }
    }
    
    
    private String update() {
        boolean success = ledgerGroupService.update(ledgerGroupDataModelJsf.getLedgerGroupDto());
        if (success) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Updated Successfully", null));
        }
        return navigateToPage();
    }
    
    private String save() {
        boolean response = ledgerGroupService.save(ledgerGroupDataModelJsf.getLedgerGroupDto());
        if (response) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully Saved", null));
        }
        return navigateToPage();
    }
    
    public String navigateToPage() {
        Utility.removeSessionBeanJSFDataModelObject("ledgerGroupDataModelJsf");
        ledgerGroupDataModelJsf = (LedgerGroupDataModelJsf) Utility.getSessionObject("ledgerGroupDataModelJsf");
        ledgerGroupDataModelJsf.setLedgerGroupDtos(ledgerGroupService.findByCollegeId(collegeDto));
        return returnToPage();
    }
    
    public String initEdit() {
        ledgerGroupDataModelJsf.setCreateEditPanel(true);
        return returnToPage();
    }
    
     public String delete() {
        ledgerGroupDataModelJsf.getLedgerGroupDto().setDeletedByAdminDto(adminDto);
        
        boolean success = ledgerGroupService.delete(ledgerGroupDataModelJsf.getLedgerGroupDto());
        if (success) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Deleted Successfully", null));
        }
        return navigateToPage();
    }
     
     
     
    
}
