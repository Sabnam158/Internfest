package com.payrollSystem.entity.common;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BillingPeriod.class)
public abstract class BillingPeriod_ extends com.payrollSystem.entity.abstracts.AbstractCode_ {

	public static volatile SingularAttribute<BillingPeriod, Integer> hierarchy;
	public static volatile SingularAttribute<BillingPeriod, Boolean> isActive;

}

