package com.payrollSystem.entity.common;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FeeGroup.class)
public abstract class FeeGroup_ extends com.payrollSystem.entity.abstracts.AbstractCode_ {

	public static volatile SingularAttribute<FeeGroup, Boolean> isActive;

}

